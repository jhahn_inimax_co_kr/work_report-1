import React, { Component } from 'react';
import { Button, Input, TimePicker, Checkbox, Icon, message } from 'antd';
import ProjectSelect from "./ProjectSelect";
import moment from 'moment';
import './WorkContent.css';

const { TextArea } = Input;
const format = 'HH:mm';
const defaultStartTime = '09:30';
const defaultEndTime = '18:30';

class WorkContent extends Component {
	state = {
		useTime: false,
		itemIndex: null,
		project: undefined,
		startTime: defaultStartTime,
		endTime: defaultEndTime,
    description: ''
	};

	projectSelect = (value) => {
		this.setState({
			project: value
		})
	};

	useTimeCheck = (e) => {
		this.setState({
			useTime: e.target.checked
		})
	};

	updateStartTime = (value) => {
		const time = `${value.hour()}:${value.minute()}`;
		this.setState({
			startTime: time
		})
	};

	updateEndTime = (value) => {
		const time = `${value.hour()}:${value.minute()}`;
		this.setState({
			endTime: time
		})
	};

	typeContent = (value) => {
	  this.setState({
      description: value
    })
  };

	clear = () => {
		this.setState({
			itemIndex: null,
			useTime: false,
			project: undefined,
			startTime: defaultStartTime,
			endTime: defaultEndTime,
      description: ''
		});
	};

	setData = () => {
		const { itemIndex, useTime, project, startTime, endTime, description } = this.state;

		let time = '-';

		if(!project) {
			message.error('프로젝트를 선택하세요.');
			return;
		}

		// 시간을 사용할때만 체크
		if(useTime) {
			const startMilliTime = moment(startTime, format).valueOf();
			const endMilliTime = moment(endTime, format).valueOf();

			if(startMilliTime >= endMilliTime) {
				message.error('FROM 시간은 END 시간보다 이전이어야합니다.');
				return;
			}
			time = `${startTime} ~ ${endTime}`;
		}

		if(!description) {
			message.error('내용을 입력하세요.');
			return;
		}

		this.props.addToList({ itemIndex, project, time, description });

		this.clear();
	};

	componentWillReceiveProps(nextProps) {
		if(nextProps.updateItem) {
			this.setState({
				useTime: nextProps.useTime,
				itemIndex: nextProps.itemIndex,
				project: nextProps.projectName,
				startTime: nextProps.useTime ? nextProps.startTime : defaultStartTime,
				endTime: nextProps.useTime ? nextProps.endTime : defaultEndTime,
        description: nextProps.content
			});
		}
	}

	render() {
		const { projects } = this.props;

		return (
			<div className="workContent">
				<h2 style={{marginTop: 15}}>
          <Icon type="form" style={{marginRight: 10}}/>Work Report
        </h2>
				{/*시간 사용 체크 박스*/}
				<div className="useTimeWrap">
					<Checkbox checked={this.state.useTime} onChange={this.useTimeCheck}>Use Time</Checkbox>
				</div>
				<div className="workReportWrap">
					{/*프로젝트 select*/}
	        <ProjectSelect projects={projects} projectSelect={this.projectSelect} project={this.state.project}/>
					{/*시간선택*/}
					<div style={{float: 'right'}}>
						{/*시간 선택*/}
						<div className="timeWrap">
							<span className="timeTitle">FROM</span>
							<TimePicker
								defaultValue={moment(defaultStartTime, format)}
								value={moment(this.state.startTime, format)}
								format={format}
								onChange={this.updateStartTime}
								disabled={!this.state.useTime}
							/>
						</div>
						<div className="timeWrap">
							<span className="timeTitle">TO</span>
							<TimePicker
								defaultValue={moment(defaultEndTime, format)}
								value={moment(this.state.endTime, format)}
								format={format}
								onChange={this.updateEndTime}
								disabled={!this.state.useTime}
							/>
						</div>
					</div>
					{/*입력칸*/}
					<div className="textContent">
						<TextArea placeholder="내용 입력" rows={5} value={this.state.description} onChange={(e) => this.typeContent(e.target.value)}/>
					</div>
					{/*추가 버튼*/}
	        <Button className="addBtn" icon="plus-circle-o" onClick={this.setData}>ADD</Button>
				</div>
			</div>
		);
	}
}

export default WorkContent;