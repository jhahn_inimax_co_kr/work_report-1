import React, { Component } from 'react';
import { Select } from 'antd';
import './ProjectSelect.css'

const Option = Select.Option;

class ProjectSelect extends Component {
	render() {
		const { projects, projectSelect, project } = this.props;
		const options = [];

		for(let i in projects) {
			let item = projects[i];
			options.push(
				<Option key={item} value={item}>{item}</Option>
			);
		}

		return (
			<div className="projectSelect">
				<Select value={project} placeholder="프로젝트 선택" style={{width: 164}} onChange={projectSelect} allowClear={true}>
					{options}
				</Select>
			</div>
		);
	}
}

export default ProjectSelect;