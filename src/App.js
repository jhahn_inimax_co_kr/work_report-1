import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import moment from 'moment';
import Login from "./routes/Login";
import Report from "./routes/Report";
import 'antd/dist/antd.min.css'

const today = moment(new Date());

class App extends Component {
  state = {
    auth: false,
    name: '',
    email: '',
    projects: []
  };

  getAuthFromStorage = () => {
    const auth = localStorage.getItem('auth') === 'true';
    if(auth) {
      const registDate = moment(localStorage.getItem('registDate'));
      const now = moment(new Date());

      const duration = moment.duration(now.diff(registDate));
      if(duration.asMinutes() > 30) return;

      const data = {
        auth: auth,
        name: localStorage.getItem('name'),
        email: localStorage.getItem('email'),
        projects: localStorage.getItem('projects').split(',')
      };

      this.setState({...data})
    }
  };

  setAuthToStorage = ({ auth, name, email, projects }) => {
    localStorage.setItem('auth', auth.toString());
    localStorage.setItem('name', name);
    localStorage.setItem('email', email);
    localStorage.setItem('projects', projects.toString());
    localStorage.setItem('registDate', new Date().toString());
  };

  // 계정 로그인
  signIn = (data) => {
    this.setState({...data});
    this.setAuthToStorage(data);
  };

  signOut = () => {
    const reset = {
      auth: false,
      name: '',
      email: '',
      projects: []
    };

    this.setState({...reset});
    this.setAuthToStorage(reset);
  };

  componentWillMount() {
    if(this.state.auth) return;
    this.getAuthFromStorage();
  }

  render() {
    const { auth, name, email, projects } = this.state;
    return (
      <Router>
        <Switch>
          <Route exact path="/login" render={(props) => <Login {...props} signIn={this.signIn}/>}/>
          <Route exact path="/main" render={(props) =>
            <Report {...props}
                    auth={auth}
                    name={name}
                    email={email}
                    projects={projects}
                    today={today}
                    signOut={this.signOut}
            />}
          />
          <Redirect from="/" to="/login" />
        </Switch>
      </Router>
    );
  }
}

export default App;
